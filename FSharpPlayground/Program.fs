﻿namespace FSharpPlayground

(* This is a very silly version of a battleship game, solitaire style.
   Hopefully you will experiment and learn more about F#! :)

Rules of the game:
 0. 60 seconds timer. Game over if the user enters coordinates but timed out.
 1. The user loses if she/he targets walls of the board.
 2. The user loses if misses any target 5 consecutive times.
 3. The user only has 30 missiles.
 4. The user loses the game if she/he runs out of missiles.
 5. Score is defined by number of missiles left * 5. If zero, show silly result.

[<-- Tasks -->]

Session 1
 0. Move the Game module to a different file.
 1. Write a function that creates battleships. One per row, 3 tiles long (horizontally).
 2. Replace the static map with a generated map based on the ships created.

Session 2
 0. Create a type to encapsulate the game state (ex.: ships created; state: in progress, won, lost; etc)
 1. Create a recursive function 'updateGame' to receive input, process it and render the updated map (game state) -> repeat.
 1.1 If the user hits a ship, sink it. Output a silly sentence unless he won the game.
 1.2 If the user misses, make fun of her/him. Remember, you want to make the user nervous.

Session 3
 0. Change the render function to use different colours (water -> blue, ship -> whatever else, sunk ship -> brown?!)
 1. How would you test this game?
 2. Go nuts. Have fun.
 
 *)

open System

module Game =

   type Health =
   | InBattle
   | Sunk

   type Battleship = { Position : int*int; State : Health }

   type Tile =
   | Ship of Battleship
   | Water

   let rnd = Random()


   // This is how a map should look like.
   // Ships are represented as ===
   let map = 
            [|
            "-------------------------------------------------------";
            "| w w w w w w w w w w w w w w w w w w w w w w w w w w |";
            "| w w w === w w w w w w w w w w w w w w w w w w w w w |";
            "| w w w w w w w w w w w w w w w w w w w w w w w w w w |";
            "| w w w w w w w w w w w === w w w w w w w w w w w w w |";
            "| w w w w w w w w w w w w w w w w w w w w w w w w w w |";
            "| w w w w w w w w w w w w w w w w w w w w w w w w w w |";
            "| w w w w w w w w w w w w w w w w w w w w w w w w w w |";
            "| w w w w w w w w w w w w w === w w w w w w w w w w w |";
            "| w w w w w w w w w w w w w w w w w w w w w w w w w w |";
            "| w w w === w w w w w w w w w w w w w w w w w w w w w |";
            "| w w w w w w w w w w w w w w w w w w w w w w w w w w |";
            "| w w w w w w w w w w w w w w w w w w w w w w w w w w |";
            "| w w w w w w w w w w w w w w w w w w w w w w w w w w |";
            "| w w w w w w w w w w w w w w w w w w w w w w w w w w |";
            "| w w w w w w w w w w w w w w w w w w w w w w w w w w |";
            "-------------------------------------------------------";
            |]

   let render (map:string[]) =
      map 
      |> Array.iter(fun tileRow -> Console.WriteLine(tileRow))

   [<EntryPoint>]
   let main argv = 

      let gameStarted = DateTime.Now

      Console.WriteLine("Welcome to the silliest battleship game to test your sense of space in the ASCII world.")

      render map

      Console.WriteLine("What row and column do you want to hit? (use values separated by comma)")

      Console.ReadLine() |> ignore

      0 // return an integer exit code